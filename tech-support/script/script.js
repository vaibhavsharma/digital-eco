
$(document).ready(function() {
	$(window).scroll(function() {
  		var scrolled = $(this).scrollTop();
  		if(scrolled > 50) {
  			$('.callUsHeader').slideDown();
  		}else{
  			$('.callUsHeader').slideUp();
  		}
  	});

  	$(document).on('click', '.readMore', function() {
  		$(this).hide();
  		$('.readMoreText').slideDown();
  	});

  	$(document).on('click', '.readLess', function() {
  		$('.readMore').show();
  		$('.readMoreText').slideUp();
  	});
});